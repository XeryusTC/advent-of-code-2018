use std::collections::{HashMap, HashSet};
use std::io::{self, BufRead};

fn manhattan(p1: &(isize, isize), p2: &(isize, isize)) -> isize {
    (p1.0 - p2.0).abs() + (p1.1 - p2.1).abs()
}

fn main() {
    let stdin = io::stdin();
    let coords: Vec<(isize, isize)> = stdin.lock().lines()
        .map(|l|
             l.unwrap().split(',').take(2)
             .map(|s| s.trim().parse::<isize>().unwrap())
             .collect::<Vec<isize>>()
        )
        .map(|c| (c[0], c[1]))
        .collect();
    let min_x = coords.iter().map(|c| c.0).min().unwrap();
    let min_y = coords.iter().map(|c| c.1).min().unwrap();
    let max_x = coords.iter().map(|c| c.0).max().unwrap();
    let max_y = coords.iter().map(|c| c.1).max().unwrap();

    let mut dists = HashMap::new();
    let mut infinite = HashSet::new();
    let mut close = 0;
    for x in min_x..max_x {
        for y in min_y..max_y {
            let mut man: Vec<(&(isize, isize), isize)> = coords.iter()
                .map(|coord| (coord, manhattan(coord, &(x, y))))
                .collect();
            man.sort_by(|(_, d1), (_, d2)| d1.cmp(d2));
            // Part 1
            if man[0].1 != man[1].1 {
                let entry = dists.entry(man[0].0).or_insert(0);
                *entry += 1;
                if x == min_x || x == max_x || y == min_y || y == max_y {
                    infinite.insert(man[0].0);
                }
            }

            // Part 2
            let s: isize = man.iter().map(|x| x.1).sum();
            if s < 10000 {
                close += 1;
            }
        }
    }
    let largest = dists.iter()
        .filter(|(k, _)| !infinite.contains(*k))
        .map(|(_, v)| v)
        .max().unwrap();
    println!("largest non-infinite: {}", largest);
    println!("size of closest region: {}", close);
}
