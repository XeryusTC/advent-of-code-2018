use std::cmp::Ordering;
use std::collections::{BTreeSet, HashMap, BinaryHeap};
use std::io::{self, BufRead};

const NUM_WORKERS: usize = 5;
const MIN_TIME: usize = 60;

type CharSet = BTreeSet<char>;

#[derive(Clone, Debug, Eq, PartialEq)]
struct Time {
    time: usize,
    worker: usize,
    step: char,
}

impl Time {
    fn new(time: usize, worker: usize, step: char) -> Time {
        Time { time, worker, step }
    }
}

impl Ord for Time {
    // Flip the cmp around so that BinaryHeap becomes a min-heap
    fn cmp(&self, other: &Time) -> Ordering {
        other.time.cmp(&self.time).then_with(|| self.worker.cmp(&other.worker))
    }
}

impl PartialOrd for Time {
    fn partial_cmp(&self, other: &Time) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn find_free_worker(workers: &[bool]) -> Option<usize> {
    for (id, &worker) in workers.iter().enumerate() {
        if !worker {
            return Some(id);
        }
    }
    None
}

fn char2duration(c: &char) -> usize {
    (c.to_digit(36).unwrap() - 'A'.to_digit(36).unwrap() + 1) as usize
        + MIN_TIME
}

fn main() {
    let stdin = io::stdin();
    let data: Vec<(char, char)> = stdin.lock().lines()
        .map(|l| {
            let l = l.unwrap();
            (l.chars().nth(5).unwrap(), l.chars().nth(36).unwrap())
        })
        .collect();
    let (left, right): (Vec<char>, Vec<char>) = data.iter().cloned().unzip();
    let steps: CharSet = left.iter().chain(right.iter())
        .cloned().collect();
    let mut completed = vec![];

    // Build dependency graph
    let mut deps: HashMap<char, Vec<char>> =
        steps.iter().map(|&c| (c, vec![])).collect();
    for (to, from) in data.iter() {
        let entry = deps.entry(*from).or_insert(vec![]);
        (*entry).push(*to);
    }

    'outer: while completed.len() != steps.len() {
        'inner: for step in steps.iter() {
            if completed.contains(&step) {
                continue;
            }
            if let Some(dep) = deps.get(&step) {
                if dep.is_empty() {
                    completed.push(step);
                    continue 'outer;
                } else if dep.iter().all(|d| completed.contains(&d)) {
                    completed.push(step);
                    continue 'outer;
                }
            }
        }
    }
    print!("Part 1: ");
    for c in completed.iter() {
        print!("{}", c);
    }
    println!("");

    // Part 2
    let mut last_time = 0;
    let mut workers = [false; NUM_WORKERS];
    let mut completed = vec![];
    let mut completion_time: BinaryHeap<Time> = BinaryHeap::new();
    let mut processing = CharSet::new();
    completion_time.push(Time::new(0, 0, ' '));

    while let Some(time) = completion_time.pop() {
        // Record the time taken
        if time.time > last_time && !completed.contains(&time.step) {
            last_time = time.time;
        }
        // Store the character in completed
        if time.step != ' ' {
            completed.push(time.step);
        }
        workers[time.worker] = false;

        for step in steps.iter() {
            if completed.contains(&step) || processing.contains(&step) {
                continue;
            }
            if let Some(dep) = deps.get(&step) {
                if dep.is_empty()
                    || dep.iter().all(|d| completed.contains(&d))
                {
                    if let Some(worker) = find_free_worker(&workers) {
                        workers[worker] = true;
                        completion_time.push(
                            Time::new(
                                time.time + char2duration(step),
                                worker,
                                *step
                            )
                        );
                        processing.insert(*step);
                    }
                }
            }
        }
    }

    println!("Time elapsed: {}", last_time);
}
