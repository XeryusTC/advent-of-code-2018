use std::io;

fn read_node<'a, I>(iter: &mut I) -> usize
where
    I: Iterator<Item = &'a usize>
{
    let children = iter.next().unwrap();
    let metadatas = iter.next().unwrap();
    let mut sum = 0;
    for _ in 0..*children {
        sum += read_node(iter);
    }
    for _ in 0..*metadatas {
        sum += iter.next().unwrap();
    }
    sum
}

fn read_node_part2<'a, I>(iter: &mut I) -> usize
where
    I: Iterator<Item = &'a usize>
{
    let children = iter.next().unwrap();
    let metadatas = iter.next().unwrap();
    let mut sum = 0;
    if *children == 0 {
        for _ in 0..*metadatas {
            sum += iter.next().unwrap();
        }
    } else {
        let mut child_sums: Vec<usize> = vec![];
        for _ in 0..*children {
            child_sums.push(read_node_part2(iter));
        }
        for _ in 0..*metadatas {
            let i = iter.next().unwrap();
            if *i == 0 || *i > child_sums.len() {
                continue;
            }
            sum = child_sums[i - 1] + sum;
        }
    }
    sum
}

fn main() {
    let mut lines = String::new();
    io::stdin().read_line(&mut lines).unwrap();
    let ns: Vec<usize> = lines.split_whitespace()
        .map(|n| n.parse::<usize>().unwrap())
        .collect();

    let sum = read_node(&mut ns.iter());
    println!("Part 1: {}", sum);

    let sum = read_node_part2(&mut ns.iter());
    println!("Part 2: {}", sum);
}
