use std::collections::{HashMap, HashSet};
use std::io::{self, BufRead};

fn main() {
    let mut twice = 0;
    let mut thrice = 0;

    let stdin = io::stdin();
    let lines: Vec<String> = stdin.lock().lines().map(|x| x.unwrap()).collect();
    for line in lines.iter() {
        let mut hist = HashMap::new();
        for c in line.chars() {
            let stat = hist.entry(c).or_insert(0);
            *stat += 1;
        }

        let mut found_two = false;
        let mut found_three = false;
        for (_, count) in &hist {
            if !found_two && *count == 2 {
                found_two = true;
                twice += 1;
            }
            if !found_three && *count == 3 {
                found_three = true;
                thrice += 1;
            }
        }
    }

    let mut ids = HashSet::new();
    for i in 0..lines.len() {
        for j in (i + 1)..lines.len() {
            let test: String = lines[i].chars()
                .zip(lines[j].chars())
                .filter(|(a, b)| a == b)
                .map(|(a, _)| a)
                .collect();
            ids.insert(test);
        }
    }
    let mut ids = ids.iter().collect::<Vec<&String>>();
    ids.sort_by(|a, b| a.len().cmp(&b.len()));

    println!("checksum: {}", (twice * thrice));
    println!("best match: {}", ids.last().unwrap());
}
