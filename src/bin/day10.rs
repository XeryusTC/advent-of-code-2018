use std::io::{self, BufRead};

#[derive(Debug)]
struct Star {
    px: isize,
    py: isize,
    vx: isize,
    vy: isize,
}

impl Star {
    fn new(px: isize, py: isize, vx: isize, vy: isize) -> Star {
        Star { px, py, vx, vy }
    }

    fn from_string(s: &str) -> Star {
        let (_, s) = s.split_at(10);
        let (px, s) = s.split_at(6);
        let (_, s) = s.split_at(2);
        let (py, s) = s.split_at(6);
        let (_, s) = s.split_at(12);
        let (vx, s) = s.split_at(2);
        let (_, s) = s.split_at(2);
        let (vy, _) = s.split_at(2);
        Star::new(
            px.trim().parse().unwrap(),
            py.trim().parse().unwrap(),
            vx.trim().parse().unwrap(),
            vy.trim().parse().unwrap(),
        )
    }

    fn step(&self) -> Star {
        Star::new(
            self.px + self.vx,
            self.py + self.vy,
            self.vx,
            self.vy,
        )
    }
}

fn draw_stars(stars: &Vec<Star>) {
    let min_x = stars.iter().map(|s| s.px).min().unwrap();
    let min_y = stars.iter().map(|s| s.py).min().unwrap();
    let max_x = stars.iter().map(|s| s.px).max().unwrap() + 1;
    let max_y = stars.iter().map(|s| s.py).max().unwrap() + 1;
    if (max_x - min_x > 200) || (max_y - min_y > 200) {
        return;
    }
    for y in min_y..max_y {
        'next_star: for x in min_x..max_x {
            for star in stars.iter() {
                if star.px == x && star.py == y {
                    print!("#");
                    continue 'next_star;
                }
            }
            print!(".");
        }
        println!();
    }
}

fn main() {
    let stdin = io::stdin();
    let mut stars: Vec<Star> = stdin.lock().lines()
        .map(|l| Star::from_string(&l.unwrap()))
        .collect();

    draw_stars(&stars);
    for i in 0..10900 {
        println!("second: {}", i + 1);
        stars = stars.iter().map(|s| s.step()).collect();
        draw_stars(&stars);
    }
}
