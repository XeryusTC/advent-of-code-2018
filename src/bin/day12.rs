use std::cmp::max;
use std::collections::HashMap;
use std::io::{self, BufRead};

fn make_iterable_state(state: &str) -> (String, usize) {
    let start = max(0, 3 - state.find('#').unwrap() as isize) as usize;
    let end = max(0,
                  6 - (state.len() - state.rfind('#').unwrap()));
    let mut new = ".".repeat(start);
    new.push_str(state);
    new.push_str(&".".repeat(end));
    (new, start)
}

fn next_state(state: &str, transitions: &HashMap<String, String>) -> String {
    let mut next = String::new();
    for i in 0..(state.len() - 5) {
        let cur = state.get(i..(i + 5)).unwrap();
        next.push_str(match transitions.get(cur) {
            Some(s) => s,
            None => ".",
        });
    }
    next
}

fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(|l| l.unwrap());
    let initial = String::from(lines.next().unwrap().split_at(15).1);
    lines.next(); // Skip the empty line
    let transitions: HashMap<String, String> = lines.map(|s| {
        let (o, s) = s.split_at(5);
        let (_, n) = s.split_at(4);
        (String::from(o), String::from(n))
    }).collect();
    println!("{}", initial);

    let (mut next, shift) = make_iterable_state(&initial);
    let mut shift = shift as isize;
    println!(" 0: {} {}", shift, next);
    for i in 0..20 {
        let (iterable, moved) = make_iterable_state(&next);
        shift += moved as isize;
        shift -= 2;
        next = next_state(&iterable, &transitions);
        println!("{:2}: {} {}", i + 1, moved, next);
    }
    println!("shifted: {}", shift);
    let sum: isize = next.chars().enumerate()
        .filter(|(_, c)| *c == '#')
        .map(|(i, _)| i as isize - shift)
        .sum();
    println!("sum: {}", sum);
}
