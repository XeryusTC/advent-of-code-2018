use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    let input: usize = stdin.lock().lines()
        .map(|l| l.unwrap().parse::<usize>())
        .nth(0).unwrap().unwrap();

    let mut scores = vec![3, 7];
    let mut score_string = String::from("37");
    let mut fst = 0;
    let mut snd = 1;

    while scores.len() < input + 10 {
        let combined = scores[fst] + scores[snd];
        if combined >= 10 {
            scores.push(1);
        }
        scores.push(combined % 10);
        score_string.push_str(&combined.to_string());
        fst = (fst + scores[fst] + 1) % scores.len();
        snd = (snd + scores[snd] + 1) % scores.len();
    }
    for score in scores.iter().skip(input).take(10) {
        print!("{}", score);
    }
    println!();

    let mut loc: Option<usize> = score_string.find(&input.to_string());
    while loc == None {
        let combined = scores[fst] + scores[snd];
        if combined >= 10 {
            scores.push(1);
        }
        scores.push(combined % 10);
        score_string.push_str(&combined.to_string());
        fst = (fst + scores[fst] + 1) % scores.len();
        snd = (snd + scores[snd] + 1) % scores.len();
        if scores.len() % 100000 == 0 {
            println!("length {}", scores.len());
            loc = score_string.find(&input.to_string());
        }
    }
    println!("first occurence: {}", loc.unwrap());
}
