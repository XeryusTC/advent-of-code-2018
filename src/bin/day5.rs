use std::io::{self, BufRead};

const LETTERS: &'static str = "abcdefghijklmnopqrstuvwxyz";

fn snip(s: &str) -> String {
    let mut out = String::new();
    let chars = s.chars().collect::<Vec<char>>();
    let mut i = 0;
    while i < chars.len() {
        if i == chars.len() - 1 {
            out.push(chars[i]);
            i += 1;
        }
        else if chars[i].eq_ignore_ascii_case(&chars[i + 1])
            && chars[i] != chars[i + 1] {
            i += 2;
        } else {
            out.push(chars[i]);
            i += 1;
        }
    }
    if out == *s {
        out
    } else {
        snip(&out)
    }
}

fn remove_all(s: &String, c: &char) -> String {
    return s.chars().filter(|x| x.to_ascii_lowercase() != *c).collect()
}

fn main() {
    let stdin = io::stdin();
    let line = stdin.lock().lines().next().unwrap().unwrap();
    println!("Part 1: {}", snip(&line).len());

    println!("Part 2: {}", LETTERS.chars()
             .map(|c| snip(&remove_all(&line, &c)).len())
             .min().unwrap());
}
