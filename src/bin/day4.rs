#[macro_use]
extern crate lazy_static;
extern crate regex;

use std::collections::HashMap;
use std::io::{self, BufRead};
use regex::Regex;

#[derive(Debug, Eq, PartialEq, PartialOrd, Ord)]
struct Time {
    month: usize,
    day: usize,
    minute: usize,
    entry: Log,
}

#[derive(Debug, Eq, PartialEq, PartialOrd, Ord)]
enum Log {
    Guard(usize),
    Sleep,
    Awake,
}

fn parse(entry: String) -> Time {
    lazy_static! {
        static ref RE: Regex = Regex::new(
            r"\[(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})\] (.*)$"
        ).unwrap();
    }
    let caps = RE.captures(&entry).unwrap();
    let log = match &caps[6] {
        "wakes up" => Log::Awake,
        "falls asleep" => Log::Sleep,
        x => {
            lazy_static! {
                static ref RE2: Regex = Regex::new(
                    r"Guard #(\d+) begins shift"
                ).unwrap();
            }
            let c = RE2.captures(&x).unwrap();
            Log::Guard(c[1].parse::<usize>().unwrap())
        }
    };
    let day_offset = if &caps[4] == "00" { 0 } else { 1 };
    let minute_offset = if &caps[4] == "00" { 60 } else { 0 };
    Time {
        month: caps[2].parse().unwrap(),
        day: caps[3].parse::<usize>().unwrap() + day_offset,
        minute: caps[5].parse::<usize>().unwrap() + minute_offset,
        entry: log
    }
}

fn main() {
    let stdin = io::stdin();
    let mut log: Vec<Time> = stdin.lock().lines()
        .map(|l| parse(l.unwrap()))
        .collect();
    log.sort();

    for l in log.iter() {
        println!("{:?}", l);
    }

    let mut schedule = HashMap::new();
    let mut last_event: &Time = log.first().unwrap();
    let mut current_guard: usize = 0;
    for entry in log.iter() {
        last_event = match entry.entry {
            Log::Guard(n) => {
                current_guard = n;
                schedule.entry(current_guard).or_insert(vec![0;120]);
                entry
            }
            Log::Sleep => {
                let s = schedule.entry(current_guard).or_insert(vec![0;120]);
                for t in (entry.minute)..120 {
                    s[t] += 1;
                }
                entry
            }
            Log::Awake => {
                let s = schedule.entry(current_guard).or_insert(vec![0;120]);
                for t in (entry.minute)..120 {
                    s[t] -= 1;
                }
                entry
            }
        };
    }

    // Part 1
    let mut most_asleep = 0;
    let mut most_minutes_asleep = 0;
    for (guard, sleep) in schedule.iter() {
        let minutes_asleep = sleep.iter().sum();
        if minutes_asleep >= most_minutes_asleep {
            most_asleep = *guard;
            most_minutes_asleep = minutes_asleep;
        }
    }

    let guard = schedule.entry(most_asleep).or_insert(vec![0;120]);
    let max = guard.iter().max().unwrap();
    for t in 0..120 {
        if guard[t] == *max {
            println!("guard {} most asleep {}: {}", most_asleep, t - 60, max);
            println!("asleep checksum: {}", most_asleep * (t - 60));
        }
    }

    // Part 2
    let mut most_often_asleep = 0;
    let mut most_often_asleep_count = 0;
    for (guard, sleep) in schedule.iter() {
        let freq = sleep.iter().max().unwrap();
        if *freq > most_often_asleep_count {
            most_often_asleep = *guard;
            most_often_asleep_count = *freq;
        }
    }

    let guard = schedule.entry(most_often_asleep).or_insert(vec![0;120]);
    for t in 0..120 {
        if guard[t] == most_often_asleep_count {
            println!("guard {} most frequest asleep on {}: {}",
                     most_often_asleep,
                     t - 60,
                     most_often_asleep_count
            );
            println!("checksum: {}",
                     most_often_asleep * (t - 60));
        }
    }
}
