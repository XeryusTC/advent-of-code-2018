use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::io::{self, BufRead, Write};

type Map = HashMap<(isize, isize), Tile>;

#[derive(Clone)]
enum Tile {
    Open,
    Tree,
    Lumberyard,
}

impl fmt::Display for Tile {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Tile::Open => write!(f, "."),
            Tile::Tree => write!(f, "|"),
            Tile::Lumberyard => write!(f, "#"),
        }
    }
}
const neighbours: [(isize, isize);8] =
    [(-1, -1), (0, -1), (1, -1),
     (-1,  0),          (1,  0),
     (-1,  1), (0,  1), (1,  1)];

fn epoch(map: Map, max_x: &isize, max_y: &isize) -> Map {
    let mut new_map = Map::new();
    for y in 0..*max_y {
        for x in 0..*max_x {
            let mut trees = 0;
            let mut lumber = 0;
            for n in neighbours.iter() {
                if let Some(tile) = map.get(&(x + n.0, y + n.1)) {
                    match tile {
                        Tile::Open => {}
                        Tile::Tree => trees += 1,
                        Tile::Lumberyard => lumber += 1,
                    };
                }
            }
            new_map.insert((x, y), match map.get(&(x, y)).unwrap() {
                Tile::Open if trees >= 3  => Tile::Tree,
                Tile::Tree if lumber >= 3 => Tile::Lumberyard,
                Tile::Lumberyard if trees == 0 || lumber == 0 => Tile::Open,
                x => x.clone(),
            });
        }
    }
    new_map
}

fn print_map(map: &Map, max_x: &isize, max_y: &isize) -> String {
    let mut result = vec![];
    for y in 0..*max_y {
        for x in 0..*max_x {
            write!(result, "{}", map.get(&(x, y)).unwrap());
        }
        writeln!(result);
    }
    String::from_utf8(result).unwrap()
}

fn count_value(map: &Map) -> isize {
    let mut tree = 0;
    let mut lumber = 0;
    for (pos, tile) in map.iter() {
        match tile {
            Tile::Open => {}
            Tile::Tree => tree += 1,
            Tile::Lumberyard => lumber += 1,
        }
    }
    tree * lumber
}

fn calculate_hash<T: Hash>(t: &T) -> u64 {
    let mut s = DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}

fn main() {
    let stdin = io::stdin();
    let mut map: Map = HashMap::new();
    for (y, line) in stdin.lock().lines().enumerate() {
        let line = line.unwrap();
        for (x, c) in line.chars().enumerate() {
            map.insert((x as isize, y as isize), match c {
                '.' => Tile::Open,
                '|' => Tile::Tree,
                '#' => Tile::Lumberyard,
                _ => panic!("Unknown character {}", c),
            });
        }
    }
    let max_x = map.keys().map(|(x, _)| x).max().unwrap() + 1;
    let max_y = map.keys().map(|(_, y)| y).max().unwrap() + 1;

    let mut transition: HashMap<u64, (u64, isize)> = HashMap::new();
    let mut completed = 0;
    // Do the epochs
    println!("{}", print_map(&map, &max_x, &max_y));
    for i in 0..1_000 {
        let previous = calculate_hash(&print_map(&map, &max_x, &max_y));
        map = epoch(map, &max_x, &max_y);
        let current = calculate_hash(&print_map(&map, &max_x, &max_y));
        match transition.get(&previous) {
            Some(_) => {
                completed = i + 1;
                break;
            }
            None => transition.insert(previous, (current, count_value(&map))),
        };
        if i == 9 {
            println!("Part 1 resource value: {}", count_value(&map));
        }
    }
    println!("Completed {} epochs", completed);

    let mut i = completed;
    let mut key = calculate_hash(&print_map(&map, &max_x, &max_y));
    let mut value = 0;
    while i < 1_000_000_000 {
        let (next, next_value) = transition.get(&key).unwrap();
        key = *next;
        value = *next_value;
        i += 1;
    }

    println!("Part 2 resource value: {}", value);
}
