extern crate binary_heap_plus;
use std::collections::{HashMap, HashSet};
use std::io;

use binary_heap_plus::*;

const NEIGHBOURS: [(isize, isize);4] = [(-1, 0), (1, 0), (0, -1), (0, 1)];
#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash, Ord, PartialOrd)]
enum Equiped {
    Torch,
    Gear,
    Neither,
}

struct Map {
    map: HashMap<(usize, usize), usize>,
    depth: usize,
}

impl Map {
    fn new(depth: usize, target: (usize, usize)) -> Map {
        let mut map = HashMap::new();
        map.insert((0, 0), depth);
        map.insert(target, depth);
        Map {
            depth,
            map,
        }
    }

    fn get(&mut self, pos: &(usize, usize)) -> usize {
        self.get_erosion(pos) % 3
    }

    fn get_erosion(&mut self, pos: &(usize, usize)) -> usize {
        match self.map.get(pos) {
            Some(val) => *val,
            None => {
                let x = pos.0;
                let y = pos.1;
                let geologic = if x == 0 && y == 0 {
                    0
                } else if x == 0 {
                    y * 48271
                } else if y == 0 {
                    x * 16807
                } else {
                    self.get_erosion(&(x - 1, y))
                        * self.get_erosion(&(x, y - 1))
                };
                let erosion = (geologic + self.depth) % 20183;
                self.map.insert(*pos, erosion);
                erosion
            }
        }
    }

    fn can_enter(&mut self, equiped: Equiped, pos: &(usize, usize)) -> bool {
        match self.get(pos) {
            0 if equiped == Equiped::Neither => false,
            1 if equiped == Equiped::Torch => false,
            2 if equiped == Equiped::Gear => false,
            0 | 1 | 2 => true,
            n => panic!("Unknown region type {}", n),
        }
    }
}

fn calculate_map(target: (usize, usize), depth: usize) -> Map {
    let mut map = Map::new(depth, target);
    for y in 0..target.1 + 1 {
        for x in 0..target.0 + 1 {
            map.get(&(x, y));
        }
    }
    map
}

fn search(map: &mut Map, target: (usize, usize)) -> usize {
    let mut hq: BinaryHeap<(usize, (usize, usize, Equiped)), MinComparator>
        = BinaryHeap::new_min();
    let mut distance: HashMap<(usize, usize, Equiped), usize> = HashMap::new();
    let mut visited: HashSet<(usize, usize, Equiped)> = HashSet::new();
    hq.push((0, (0, 0, Equiped::Torch)));
    while let Some((dist, (x, y, equiped))) = hq.pop() {
        // If we're in the goal position and we hold the toch then we're done
        if x == target.0 && y == target.1 && equiped == Equiped::Torch {
            return dist;
        }
        // Skip already visited nodes
        if visited.contains(&(x, y, equiped)) {
            continue;
        }
        visited.insert((x, y, equiped));
        for n in NEIGHBOURS.iter() {
            // Skip impassable negative x and y
            if (x == 0 && n.0 < 0) || (y == 0 && n.1 < 0) {
                continue;
            }
            // Add the neighbours (if it is possible to go there)
            let nx = (x as isize + n.0) as usize;
            let ny = (y as isize + n.1) as usize;
            match distance.get(&(nx, ny, equiped)) {
                Some(v) => {
                    if *v > dist + 1 && map.can_enter(equiped, &(nx, ny))
                        && !visited.contains(&(nx, ny, equiped)) {
                        hq.push((dist + 1, (nx, ny, equiped)));
                        distance.insert((nx, ny, equiped), dist + 1);
                    }
                }
                None => {
                    if map.can_enter(equiped, &(nx, ny))
                        && !visited.contains(&(nx, ny, equiped)) {
                        hq.push((dist + 1, (nx, ny, equiped)));
                        distance.insert((nx, ny, equiped), dist + 1);
                    }
                }
            }
            // Swap equipment
            for eq in [Equiped::Torch, Equiped::Gear, Equiped::Neither].iter() {
                if *eq != equiped && map.can_enter(*eq, &(x, y))
                        && !visited.contains(&(nx, ny, *eq)) {
                    hq.push((dist + 7, (x, y, *eq)));
                    distance.insert((x, y, *eq), dist + 7);
                }
            }
        }
    }
    0
}

fn main() {
    let stdin = io::stdin();
    let mut input = String::new();
    stdin.read_line(&mut input).unwrap();
    let depth = input.chars().skip(7).collect::<String>().trim()
        .parse::<usize>().unwrap();

    input = String::new();
    stdin.read_line(&mut input).unwrap();
    let pos = input.chars().skip(8).collect::<String>().trim().split(',')
        .map(|v| v.trim().parse::<usize>().unwrap()).collect::<Vec<usize>>();

    let mut map = calculate_map((pos[0], pos[1]), depth);
    let risk: usize = map.map.values().map(|v| v % 3).sum();
    println!("Risk level: {}", risk);
    println!("Search time: {}", search(&mut map, (pos[0], pos[1])));
}
