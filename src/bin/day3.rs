#[macro_use]
extern crate lazy_static;
extern crate regex;

use std::collections::HashMap;
use std::io::{self, BufRead};

use regex::Regex;

#[derive(Debug)]
struct Claim {
    id: usize,
    x: usize,
    y: usize,
    width: usize,
    height: usize,
}

impl Claim {
    fn new(id: usize,
           x: usize,
           y: usize,
           width: usize,
           height: usize)
        -> Claim
    {
        return Claim { id, x, y, width, height };
    }

    fn from_string(s: &String) -> Claim {
        lazy_static! {
            static ref re: Regex = Regex::new(
                r"#(?P<id>\d+) @ (?P<x>\d+),(?P<y>\d+): (?P<width>\d+)x(?P<height>\d+)"
            ).unwrap();
        }
        let caps = re.captures(s).unwrap();
        return Claim::new(caps["id"].parse::<usize>().unwrap(),
                          caps["x"].parse::<usize>().unwrap(),
                          caps["y"].parse::<usize>().unwrap(),
                          caps["width"].parse::<usize>().unwrap(),
                          caps["height"].parse::<usize>().unwrap()
        );
    }

    fn collide(&self, other: &Claim) -> bool {
        return self.x < other.x + other.width &&
               self.x + self.width > other.x &&
               self.y < other.y + other.height &&
               self.y + self.height > other.y;
    }
}

fn main() {
    let stdin = io::stdin();
    let lines: Vec<String> = stdin.lock().lines().map(|x| x.unwrap()).collect();
    let claims: Vec<Claim> = lines.iter().map(Claim::from_string).collect();

    let mut fabric = HashMap::new();
    for claim in claims.iter() {
        for x in claim.x..(claim.x + claim.width) {
            for y in claim.y..(claim.y + claim.height) {
                let e = fabric.entry((x, y)).or_insert(0);
                *e += 1;
            }
        }
    }
    let more_than_one = fabric.iter().filter(|(_, &x)| x > 1).count();
    println!("count: {}", more_than_one);

    'outer: for c1 in claims.iter() {
        for c2 in claims.iter() {
            if c1.id == c2.id {
                continue;
            }
            if c1.collide(&c2) {
                continue 'outer;
            }
        }
        println!("single: {}", c1.id);
    }
}
