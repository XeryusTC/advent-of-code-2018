#[macro_use]
extern crate lazy_static;
extern crate regex;

use std::collections::{HashMap, HashSet};
use std::io;
use regex::Regex;

#[derive(Debug)]
struct Sample {
    before: Box<[usize]>,
    op: Box<[usize]>,
    after: Box<[usize]>,
}

impl Sample {
    fn from_lines(before: &str, op: &str, after: &str) -> Sample {
        lazy_static! {
            static ref before_re: Regex = Regex::new(
                r"Before: \[(\d+), (\d+), (\d+), (\d+)\]").unwrap();
            static ref op_re: Regex = Regex::new(
                r"(\d+) (\d+) (\d+) (\d+)").unwrap();
            static ref after_re: Regex = Regex::new(
                r"After:  \[(\d+), (\d+), (\d+), (\d+)\]").unwrap();
        }
        let before_cap = before_re.captures(before).unwrap();
        let op_cap = op_re.captures(op).unwrap();
        let after_cap = after_re.captures(after).unwrap();

        let before = Box::new([
            before_cap.get(1).unwrap().as_str().parse::<usize>().unwrap(),
            before_cap.get(2).unwrap().as_str().parse::<usize>().unwrap(),
            before_cap.get(3).unwrap().as_str().parse::<usize>().unwrap(),
            before_cap.get(4).unwrap().as_str().parse::<usize>().unwrap(),
        ]);
        let op = Box::new([
            op_cap.get(1).unwrap().as_str().parse::<usize>().unwrap(),
            op_cap.get(2).unwrap().as_str().parse::<usize>().unwrap(),
            op_cap.get(3).unwrap().as_str().parse::<usize>().unwrap(),
            op_cap.get(4).unwrap().as_str().parse::<usize>().unwrap(),
        ]);
        let after = Box::new([
            after_cap.get(1).unwrap().as_str().parse::<usize>().unwrap(),
            after_cap.get(2).unwrap().as_str().parse::<usize>().unwrap(),
            after_cap.get(3).unwrap().as_str().parse::<usize>().unwrap(),
            after_cap.get(4).unwrap().as_str().parse::<usize>().unwrap(),
        ]);
        Sample { before, op, after }
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
enum OpCodes {
    addr, addi,
    mulr, muli,
    banr, bani,
    borr, bori,
    setr, seti,
    gtir, gtri, gtrr,
    eqir, eqri, eqrr,
}

#[derive(Debug, Eq, PartialEq)]
struct State {
    registers: Box<[usize]>,
}

impl State {
    fn new(registers: Box<[usize]>) -> State {
        State { registers }
    }

    fn op(&self, opcode: &OpCodes, op: Box<[usize]>) -> State {
        let mut r = self.registers.clone();
        r[op[3]] = match opcode {
            OpCodes::addr => r[op[1]] + r[op[2]],
            OpCodes::addi => r[op[1]] + op[2],
            OpCodes::mulr => r[op[1]] * r[op[2]],
            OpCodes::muli => r[op[1]] * op[2],
            OpCodes::banr => r[op[1]] & r[op[2]],
            OpCodes::bani => r[op[1]] & op[2],
            OpCodes::borr => r[op[1]] | r[op[2]],
            OpCodes::bori => r[op[1]] | op[2],
            OpCodes::setr => r[op[1]],
            OpCodes::seti => op[1],
            OpCodes::gtir => if   op[1]  >  r[op[2]] { 1 } else { 0 },
            OpCodes::gtri => if r[op[1]] >    op[2]  { 1 } else { 0 },
            OpCodes::gtrr => if r[op[1]] >  r[op[2]] { 1 } else { 0 },
            OpCodes::eqir => if   op[1]  == r[op[2]] { 1 } else { 0 },
            OpCodes::eqri => if r[op[1]] ==   op[2]  { 1 } else { 0 },
            OpCodes::eqrr => if r[op[1]] == r[op[2]] { 1 } else { 0 },
        };
        State { registers: r }
    }
}

fn distil_opcodes(poss: HashMap<usize, HashSet<OpCodes>>)
    -> HashMap<usize, HashSet<OpCodes>>
{
    let mut new_map: HashMap<usize, HashSet<OpCodes>> = HashMap::new();
    for (code, op) in poss.iter() {
        if op.len() == 1 {
            new_map.insert(*code, op.clone());
            for (other_code, other_op) in poss.iter() {
                if code == other_code {
                    continue;
                }
                let e = new_map.entry(*other_code).or_insert(other_op.clone());
                for op in op.iter() {
                    (*e).remove(op);
                }
            }
        }
    }
    if new_map.iter().any(|(_, op)| op.len() > 1) {
        distil_opcodes(new_map)
    } else {
        new_map
    }
}

fn cleanup(map: HashMap<usize, HashSet<OpCodes>>) -> HashMap<usize, OpCodes> {
    map.iter()
        .map(|(&k, v)| (k, v.iter().collect::<Vec<&OpCodes>>()[0].clone()))
        .collect()
}

fn main() {
    let stdin = io::stdin();
    let mut samples: Vec<Sample> = vec![];
    let mut master_count = 0;
    let opcodes = [OpCodes::addr, OpCodes::addi, OpCodes::mulr, OpCodes::muli,
                   OpCodes::banr, OpCodes::bani, OpCodes::borr, OpCodes::bori,
                   OpCodes::setr, OpCodes::seti, OpCodes::gtir, OpCodes::gtri,
                   OpCodes::gtrr, OpCodes::eqir, OpCodes::eqri, OpCodes::eqrr
    ].iter().cloned().collect::<HashSet<OpCodes>>();
    let mut instructions: Vec<Box<[usize]>> = vec![];

    let mut opcode_poss: HashMap<usize, HashSet<OpCodes>> = HashMap::new();
    for op in 0..16 {
        opcode_poss.entry(op).or_insert(opcodes.clone());
    }

    loop {
        let mut line = String::new();
        let read = stdin.read_line(&mut line).unwrap(); 
        if read == 0 {
            break;
        }
        if line.starts_with("Before") {
            let mut op = String::new();
            let mut after = String::new();
            let before = line;
            stdin.read_line(&mut op).unwrap();
            stdin.read_line(&mut after).unwrap();
            samples.push(Sample::from_lines(&before, &op, &after));
        } else if line == "\n" {
        } else {
            lazy_static! {
                static ref re: Regex = Regex::new(r"(\d+) (\d+) (\d+) (\d+)")
                    .unwrap();
            }
            let cap = re.captures(&line).unwrap();
            instructions.push(Box::new([
                cap.get(1).unwrap().as_str().parse::<usize>().unwrap(),
                cap.get(2).unwrap().as_str().parse::<usize>().unwrap(),
                cap.get(3).unwrap().as_str().parse::<usize>().unwrap(),
                cap.get(4).unwrap().as_str().parse::<usize>().unwrap(),
            ]));
        }
    }

    for sample in samples.iter() {
        let mut count = 0;
        for op in opcodes.iter() {
            let before = State::new(sample.before.clone());
            let after = State::new(sample.after.clone());
            if before.op(op, sample.op.clone()) == after {
                count += 1;
            } else {
                let e = opcode_poss.entry(sample.op[0])
                    .or_insert(opcodes.clone());
                (*e).remove(op);
            }
        }
        if count >= 3 {
            master_count += 1;
        }
    }

    println!("count: {}", master_count);
    let opcodes = cleanup(distil_opcodes(opcode_poss));

    let mut state = State::new(Box::new([0, 0, 0, 0]));
    for op in instructions.iter() {
        state = state.op(opcodes.get(&op[0]).unwrap(), op.clone());
    }
    println!("{:?}", state);
}
