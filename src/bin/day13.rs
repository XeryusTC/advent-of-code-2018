use std::collections::{HashMap, BTreeSet};
use std::fmt;
use std::io::{self, BufRead};

// True = part 2, false = part 1
const IGNORE_CRASHES: bool = true;

#[derive(Debug)]
enum Tile {
    Horizontal,
    Vertical,
    Crossing,
    CornerNE, // \
    CornerNW, // /
    CornerSE, // /
    CornerSW, // \
}

impl Tile {
    fn from_char(cur: &char, prev: &Option<char>) -> Option<Tile> {
        match *cur {
            '-' | '<' | '>' => Some(Tile::Horizontal),
            '|' | 'v' | '^' => Some(Tile::Vertical),
            '+' => Some(Tile::Crossing),
            '/' => {
                match prev {
                    Some(prev) => {
                        match prev {
                            ' ' | '|' | '/' => Some(Tile::CornerNW),
                            '-' | '>' | '+' | '<' => Some(Tile::CornerSE),
                            _ => panic!("unmatched {}{}", prev, cur),
                        }
                    },
                    None => Some(Tile::CornerNW),
                }
            }
            '\\' => {
                match prev {
                    Some(prev) => {
                        match prev {
                            ' ' | '|' | '/' => Some(Tile::CornerSW),
                            '-' | '>' | '+' | '<' => Some(Tile::CornerNE),
                            _ => panic!("unmatched {}{}", prev, cur),
                        }
                    },
                    None => Some(Tile::CornerSW),
                }
            }
            _ => None,
        }
    }
}

impl fmt::Display for Tile {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self {
            Tile::Horizontal => '-',
            Tile::Vertical => '|',
            Tile::Crossing => '+',
            Tile::CornerNE => '⌝',
            Tile::CornerNW => '⌜',
            Tile::CornerSW => '⌞',
            Tile::CornerSE => '⌟',
        })
    }
}

#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl fmt::Display for Direction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self {
            Direction::Up => '^',
            Direction::Down => 'v',
            Direction::Left => '<',
            Direction::Right => '>',
        })
    }
}

#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
enum Choice {
    Left,
    Straight,
    Right,
}

#[derive(Clone, Debug, Eq, Ord, PartialOrd)]
struct Cart {
    // Swap x and y around for sorting
    y: usize,
    x: usize,
    direction: Direction,
    crossing: Choice,
}

impl Cart {
    fn new(x: usize, y: usize, direction: Direction, crossing: Choice) -> Cart {
        Cart { x, y, direction, crossing }
    }

    fn from_char(cur: &char, x: &usize, y: &usize) -> Option<Cart> {
        let direction = match cur { 
            '^' => Direction::Up,
            'v' => Direction::Down,
            '<' => Direction::Left,
            '>' => Direction::Right,
            _ => return None,
        };
        Some(Cart {
            direction,
            x: *x,
            y: *y,
            crossing: Choice::Left,
        })
    }

    fn move_up(&self) -> Cart {
        Cart::new(self.x, self.y - 1, Direction::Up, self.crossing.clone())
    }

    fn move_down(&self) -> Cart {
        Cart::new(self.x, self.y + 1, Direction::Down, self.crossing.clone())
    }

    fn move_left(&self) -> Cart {
        Cart::new(self.x - 1, self.y, Direction::Left, self.crossing.clone())
    }

    fn move_right(&self) -> Cart {
        Cart::new(self.x + 1, self.y, Direction::Right, self.crossing.clone())
    }

    fn move_crossing(&self) -> Cart {
        let cart = match self.crossing {
            Choice::Left => {
                match self.direction {
                    Direction::Up => self.move_left(),
                    Direction::Down => self.move_right(),
                    Direction::Left => self.move_down(),
                    Direction::Right => self.move_up(),
                }
            }
            Choice::Straight => {
                match self.direction {
                    Direction::Up => self.move_up(),
                    Direction::Down => self.move_down(),
                    Direction::Left => self.move_left(),
                    Direction::Right => self.move_right(),
                }
            }
            Choice::Right => {
                match self.direction {
                    Direction::Up => self.move_right(),
                    Direction::Down => self.move_left(),
                    Direction::Left => self.move_up(),
                    Direction::Right => self.move_down(),
                }
            }
        };
        cart.next_choice()
    }

    fn next_choice(self) -> Cart {
        match self.crossing {
            Choice::Left => Cart { crossing: Choice::Straight, ..self },
            Choice::Straight => Cart { crossing: Choice::Right, ..self },
            Choice::Right => Cart { crossing: Choice::Left, ..self } ,
        }
    }
}

impl fmt::Display for Cart {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.direction)
    }
}

impl PartialEq for Cart {
    fn eq(&self, other: &Cart) -> bool {
        self.x == other.x && self.y == other.y
    }
}

fn draw_map(
    tiles: &HashMap<(usize, usize), Tile>,
    carts: &BTreeSet<Cart>,
    width: &usize,
    height: &usize
) {
    for y in 0..*height {
        'cell: for x in 0..*width {
            for cart in carts.iter() {
                if cart.x == x && cart.y == y {
                    print!("{}", cart);
                    continue 'cell;
                }
            }
            print!("{}", match tiles.get(&(x, y)) {
                None => String::from(" "),
                Some(tile) => format!("{}", tile),
            });
        }
        println!();
    }
}

fn tick(
    tiles: &HashMap<(usize, usize), Tile>,
    carts: &BTreeSet<Cart>,
) -> Result<BTreeSet<Cart>, (usize, usize)> {
    let mut new_carts: BTreeSet<Cart> = BTreeSet::new();
    let mut remove_carts: BTreeSet<Cart> = BTreeSet::new();
    'cart: for cart in carts.iter() {
        // Check if something moved on top of us
        for other_cart in new_carts.iter() {
            if cart.x == other_cart.x && cart.y == other_cart.y {
                if IGNORE_CRASHES {
                    remove_carts.insert(other_cart.clone());
                    continue 'cart;
                } else {
                    return Err((cart.x, cart.y));
                }
            }
        }
        // Do the moving
        let tile = tiles.get(&(cart.x, cart.y)).unwrap();
        let new_cart = match cart.direction {
            Direction::Up => match tile {
                Tile::Vertical => cart.move_up(),
                Tile::CornerNW => cart.move_right(),
                Tile::CornerNE => cart.move_left(),
                Tile::Crossing => cart.move_crossing(),
                _ => panic!("Cart can't point up and be on a south corner"),
            },
            Direction::Down => match tile {
                Tile::Vertical => cart.move_down(),
                Tile::CornerSW => cart.move_right(),
                Tile::CornerSE => cart.move_left(),
                Tile::Crossing => cart.move_crossing(),
                _ => panic!("Cart can't point down and be on a north corner"),
            },
            Direction::Right => match tile {
                Tile::Horizontal => cart.move_right(),
                Tile::CornerNE => cart.move_down(),
                Tile::CornerSE => cart.move_up(),
                Tile::Crossing => cart.move_crossing(),
                _ => panic!("Cart can't go right and be on a west corner"),
            },
            Direction::Left => match tile {
                Tile::Horizontal => cart.move_left(),
                Tile::CornerNW => cart.move_down(),
                Tile::CornerSW => cart.move_up(),
                Tile::Crossing => cart.move_crossing(),
                _ => panic!("Cart can't go left and be on a east corner"),
            },
        };
        // Check if we moved on top of something
        for other_cart in new_carts.iter() {
            if new_cart.x == other_cart.x && new_cart.y == other_cart.y {
                if IGNORE_CRASHES {
                    remove_carts.insert(other_cart.clone());
                    continue 'cart;
                } else {
                    return Err((new_cart.x, new_cart.y));
                }
            }
        }
        new_carts.insert(new_cart);
    }
    Ok(new_carts.difference(&remove_carts).cloned().collect())
}

fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines().map(|l| l.unwrap());

    let mut map = HashMap::new();
    let mut carts = BTreeSet::new();
    let mut width = 0;
    let mut height = 0;
    for (y, line) in lines.enumerate() {
        height += 1;
        if line.len() > width {
            width = line.len();
        }
        let mut prev = None;
        for (x, c) in line.chars().enumerate() {
            if let Some(tile) = Tile::from_char(&c, &prev) {
                map.insert((x, y), tile);
            }
            prev = Some(c);

            if let Some(cart) = Cart::from_char(&c, &x, &y) {
                carts.insert(cart);
            }
        }
    }
    println!("{:?}", map);
    loop {
        carts = match tick(&map, &carts) {
            Ok(c) => c,
            Err((x, y)) => {
                println!("Crash at {},{}", x, y);
                return;
            }
        };
        println!("{:?}", carts);
        if carts.len() == 1 {
            break;
        }
    }
}
