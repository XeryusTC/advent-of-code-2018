extern crate linked_list;

use linked_list::{Cursor, LinkedList};

const NUM_PLAYERS: usize = 448;
const MARBLES: usize = 71628 * 100;

fn seek<T>(cursor: &mut Cursor<T>, n: usize) {
    for _ in 0..n {
        match cursor.next() {
            Some(_) => {}
            None => { cursor.next(); },
        }
    }
}

fn prev<T>(cursor: &mut Cursor<T>, n: usize) {
    for _ in 0..n {
        match cursor.prev() {
            Some(_) => {}
            None => { cursor.prev(); }
        }
    }
}

fn main() {
    let mut marbles = LinkedList::new();
    marbles.push_back(0);
    let mut cursor = marbles.cursor();
    let mut scores = [0; NUM_PLAYERS];
    for i in 1..(MARBLES + 1) {
        if i % 23 == 0 {
            prev(&mut cursor, 7);
            let removed = cursor.remove().unwrap();
            scores[(i - 1) % NUM_PLAYERS] += i + removed;
        } else {
            seek(&mut cursor, 2);
            cursor.insert(i);
        }
    }
    println!("high score: {}", scores.iter().max().unwrap());
}
