#[macro_use]
extern crate lazy_static;
extern crate regex;

use std::collections::HashSet;
use std::io::{self, BufRead};

use regex::Regex;

type Coord = (usize, usize);
type Coords = HashSet<Coord>;

#[derive(Debug)]
enum EndType {
    Wall(Coord),
    Source(Coord),
}

fn parse_line(line: &str) -> Coords {
    lazy_static! {
        static ref re1: Regex = Regex::new(r"^(.)=(\d+)").unwrap();
        static ref re2: Regex = Regex::new(r"^(.)=(\d+)..(\d+)").unwrap();
    }
    let parts: Vec<&str> = line.split_whitespace().collect();
    let caps1 = re1.captures(parts[0]).unwrap();
    let caps2 = re2.captures(parts[1]).unwrap();
    let n = caps1[2].parse::<usize>().unwrap();
    let i_start = caps2[2].parse::<usize>().unwrap();
    let i_end = caps2[3].parse::<usize>().unwrap() + 1;
    match &caps1[1] {
        "x" => (i_start..i_end).map(|i| (n, i)).collect(),
        "y" => (i_start..i_end).map(|i| (i, n)).collect(),
        c => panic!("Unknown coordinate option {}", c),
    }
}

fn simulate<'a>(
    map: &Coords,
    water: &'a mut Coords,
    start: &Coord,
    max_y: &usize) -> Result<(&'a mut Coords, Coords), Coords>
{
    println!("Starting simulation at {:?}", start);
    let mut sources = Coords::new();
    'outer: for y in (start.1 + 1).. {
        if y > *max_y {
            return Err((start.1..y).map(|y| (start.0, y)).collect());
        }
        if !map.contains(&(start.0, y)) {
            water.insert((start.0, y));
        } else {
            for raise in 1.. {
                let (left, right) = find_walls(map,
                                               &water,
                                               (start.0, y - raise));
                let min = match left {
                    EndType::Source(coord) => {
                        sources.insert(coord);
                        coord
                    }
                    EndType::Wall(coord) => coord,
                };
                let max = match right {
                    EndType::Source(coord) => {
                        sources.insert(coord);
                        water.insert(coord);
                        coord
                    }
                    EndType::Wall(coord) => coord,
                };
                for x in (min. 0 + 1)..max.0 {
                    water.insert((x, y - raise));
                }
                if sources.len() > 0 {
                    break 'outer;
                }
            }
        }
    }
    Ok((water, sources))
}

fn find_walls(map: &Coords,
              filled: &Coords,
              pos: Coord) -> (EndType, EndType)
{
    let mut i = 1;
    let mut continue_min = true;
    let mut continue_max = true;
    let mut min = EndType::Wall(pos);
    let mut max = EndType::Wall(pos);
    while continue_min || continue_max {
        let coord: Coord = (pos.0 - i, pos.1);
        let floor: Coord = (pos.0 - i, pos.1 + 1);
        if continue_min {
            if map.contains(&coord) {
                continue_min = false;
                min = EndType::Wall(coord);
            } else if !map.contains(&floor) && !filled.contains(&floor) {
                continue_min = false;
                min = EndType::Source(coord);
            }
        }

        let coord: Coord = (pos.0 + i, pos.1);
        let floor: Coord = (pos.0 + i, pos.1 + 1);
        if continue_max {
            if map.contains(&coord) {
                continue_max = false;
                max = EndType::Wall(coord);
            } else if !map.contains(&floor) && !filled.contains(&floor) {
                continue_max = false;
                max = EndType::Source(coord);
            }
        }
        i += 1;
    }
    println!("walls {:?} {:?}", min, max);
    (min, max)
}

fn run(map: &Coords,
       water: &mut Coords,
       collected_sources: &Coords,
       start: &Coord,
       max_y: &usize,
       depth: usize) -> Coords {
    match simulate(map, water, start, max_y) {
        Ok((filled, sources)) => {
            let mut crossed = filled.clone();
            if depth < 100 {
                for source in sources.iter() {
                    if collected_sources.contains(&source) {
                        continue;
                    }
                    crossed = crossed
                        .union(&run(map,
                                    water,
                                    &collected_sources.union(&sources)
                                        .cloned().collect(),
                                    source,
                                    max_y,
                                    depth + 1))
                        .cloned().collect();
                }
            }
            crossed
        }
        Err(filled) => filled,
    }
}

fn print_map(map: &Coords, crossed: Option<&Coords>) {
    let min_x = map.iter().map(|(x, _)| x).min().unwrap();
    let max_x = map.iter().map(|(x, _)| x).max().unwrap();
    let max_y = map.iter().map(|(_, y)| y).max().unwrap();
    for y in 0..(max_y + 1) {
        for x in *min_x..max_x+1 {
            if x == 500 && y == 0 {
                print!("+");
                continue;
            }
            if let Some(crossed) = crossed {
                if crossed.contains(&(x, y)) {
                    print!("-");
                } else if map.contains(&(x, y)) {
                 print!("#");
                } else {
                    print!(" ");
                }
            } else {
                if map.contains(&(x, y)) {
                    print!("#");
                } else {
                    print!(" ");
                }
            }
        }
        println!();
    }
}

fn main() {
    let stdin = io::stdin();
    let map: Coords = stdin.lock().lines()
        .flat_map(|l| parse_line(&l.unwrap()))
        .collect();
    let max_y = map.iter().map(|(_, y)| y).max().unwrap();
    //println!("map {:?}, {}", map, max_y);
    //print_map(&map, None);

    let crossed = run(&map,
                      &mut Coords::new(),
                      &Coords::new(),
                      &(500, 0),
                      &max_y,
                      0);
    print_map(&map, Some(&crossed));
    println!("crossed {}", crossed.len());
}
