use std::collections::HashSet;
use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    let shifts = stdin.lock().lines()
        .map(|x| x.unwrap().parse::<i32>().unwrap()).collect::<Vec<i32>>();
    println!("final count {}", shifts.iter().sum::<i32>());

    // Repeat the frequencies until we hit a repetition
    let mut count = 0;
    let mut encountered = HashSet::new();
    let mut i = 0;
    loop {
        count += shifts[i % shifts.len()];
        if !encountered.insert(count) {
            println!("repeat {}: {}", i, count);
            break;
        }
        i += 1;
    }
}
