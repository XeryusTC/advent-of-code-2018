use std::io::{self, BufRead};

fn power_level(x: isize, y: isize, serial: &isize) -> isize {
    let id = x + 10;
    (((id * y) + serial) * id / 100 % 10) - 5
}

fn power_square(x: &isize, y: &isize, size: &isize, serial: &isize) -> isize {
    let mut sum = 0;
    for i in 0..*size {
        for j in 0..*size {
            sum += power_level(x + i, y + j, serial)
        }
    }
    sum
}

fn main() {
    let stdin = io::stdin();
    let serial = stdin.lock().lines()
        .nth(0).unwrap().unwrap()
        .parse::<isize>().unwrap();
    let mut max_x = 0;
    let mut max_y = 0;
    let mut max = 0;
    for x in 1..298 {
        for y in 1..298 {
            let power = power_square(&x, &y, &3, &serial);
            if power > max {
                max = power;
                max_x = x;
                max_y = y;
            }
        }
    }
    println!("{},{}", max_x, max_y);

    let mut max_x = 0;
    let mut max_y = 0;
    let mut max_size = 0;
    let mut max = 0;

    for size in 1..300 {
        println!("Checking size {}", size);
        for x in 1..(301 - size) {
            for y in 1..(301 - size) {
                let power = power_square(&x, &y, &size, &serial);
                if power > max {
                    max = power;
                    max_x = x;
                    max_y = y;
                    max_size = size;
                }
            }
        }
        println!("{},{},{}", max_x, max_y, max_size);
    }
    println!("{},{},{}", max_x, max_y, max_size);
}
